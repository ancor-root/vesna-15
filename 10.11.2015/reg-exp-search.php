<?php

$html = <<<'HTML'

<dd>Focus search box</dd>
</dl></div></nav>
<div id="goto">
    <div class="search">
         <div class="text"></div>
         <div class="results"><ul></ul></div>
   </div>
</div>

  <div id="breadcrumbs" class="clearfix">
    <div id="breadcrumbs-inner">
          <div class="next">
        <a href="function.preg-quote.php">
          preg_quote &raquo;
        </a>
      </div>
              <div class="prev">
        <a href="function.preg-match-all.php">
          &laquo; preg_match_all        </a>
      </div>
          <ul>
            <li><a href='index.php'>Руководство по PHP</a></li>      <li><a href='funcref.php'>Справочник функций</a></li>      <li><a href='refs.basic.text.php'>Обработка текста</a></li>      <li><a href='book.pcre.php'>PCRE</a></li>      <li><a href='ref.pcre.php'>PCRE</a></li>      </ul>
    </div>
  </div>




<div id="layout" class="clearfix">
  <section id="layout-content">
  <div class="page-tools">
    <div class="change-language">
      <form action="/manual/change.php" method="get" id="changelang" name="changelang">
        <fieldset>
          <label for="changelang-langs">Change language:</label>
          <select onchange="document.changelang.submit()" name="page" id="changelang-langs">
            <option value='en/function.preg-match.php'>English</option>
            <option value='pt_BR/function.preg-match.php'>Brazilian Portuguese</option>
            <option value='zh/function.preg-match.php'>Chinese (Simplified)</option>
            <option value='fr/function.preg-match.php'>French</option>
            <option value='de/function.preg-match.php'>German</option>
            <option value='ja/function.preg-match.php'>Japanese</option>
            <option value='kr/function.preg-match.php'>Korean</option>
            <option value='ro/function.preg-match.php'>Romanian</option>
            <option value='ru/function.preg-match.php' selected="selected">Russian</option>
            <option value='es/function.preg-match.php'>Spanish</option>
            <option value='tr/function.preg-match.php'>Turkish</option>
            <option value="help-translate.php">Other</option>
          </select>
        </fieldset>
      </form>
    </div>
    <div class="edit-bug">
      <a href="https://edit.php.net/?project=PHP&amp;perm=ru/function.preg-match.php">Edit</a>
      <a href="https://bugs.php.net/report.php?bug_type=Documentation+problem&amp;manpage=function.preg-match">Report a Bug</a>
    </div>
  </div><div id="function.preg-match" class="refentry">
 <div class="refnamediv">
  <h1 class="refname">preg_match</h1>
  <p class="verinfo">(PHP 4, PHP 5)</p><p class="refpurpose"><span class="refname">preg_match</span> &mdash; <span class="dc-title">Выполняет проверку на соответствие регулярному выражению</span></p>

 </div>

 <div class="refsect1 description" id="refsect1-function.preg-match-description">
  <h3 class="title">Описание</h3>
  <div class="methodsynopsis dc-description">
   <span class="type">int</span> <span class="methodname"><strong>preg_match</strong></span>
    ( <span class="methodparam"><span class="type">string</span> <code class="parameter">$pattern</code></span>
   , <span class="methodparam"><span class="type">string</span> <code class="parameter">$subject</code></span>
   [, <span class="methodparam"><span class="type">array</span> <code class="parameter reference">&$matches</code></span>
   [, <span class="methodparam"><span class="type">int</span> <code class="parameter">$flags</code><span class="initializer"> = 0</span></span>
   [, <span class="methodparam"><span class="type">int</span> <code class="parameter">$offset</code><span class="initializer"> = 0</span></span>
  ]]] )</div>

  <p class="para rdfs-comment">
   Ищет в заданном тексте <code class="parameter">subject</code> совпадения
   с шаблоном <code class="parameter">pattern</code>.
  </p>
 </div>


 <div class="refsect1 parameters" id="refsect1-function.preg-match-parameters">
  <h3 class="title">Список параметров</h3>
  <p class="para">
   <dl>

    
     <dt>
<code class="parameter">pattern</code></dt>

     <dd>

      <p class="para">
       Искомый шаблон, строка.
      </p>
     </dd>

    
    
     <dt>
<code class="parameter">subject</code></dt>

     <dd>

      <p class="para">
       Входная строка.
      </p>
     </dd>

    
    
     <dt>
<code class="parameter">matches</code></dt>

     <dd>

      <p class="para">
       В случае, если указан дополнительный параметр
       <code class="parameter">matches</code>, он будет заполнен
       результатами поиска. Элемент <var class="varname"><var class="varname">$matches[0]</var></var>
       будет содержать часть строки, соответствующую вхождению
       всего шаблона, <var class="varname"><var class="varname">$matches[1]</var></var> - часть строки,
       соответствующую первой подмаске, и так далее.
      </p>
     </dd>

    
    
     <dt>
<code class="parameter">flags</code></dt>

     <dd>

      <p class="para">
       <code class="parameter">flags</code> может принимать значение следующего флага:
       <dl>

        
         <dt>
<strong><code>PREG_OFFSET_CAPTURE</code></strong></dt>

         <dd>

          <span class="simpara">
           В случае, если этот флаг указан, для каждой найденной
           подстроки будет указана ее позиция в исходной строке.
           Необходимо помнить, что этот флаг меняет формат
           возвращаемого массива <code class="parameter">matches</code> в

HTML;


/*
() - активная
(?:sdfasf) - пассивная
*/

$pattern = '/\<a.+href=[\'"]([^\'"]+)[\'"].+\>/U';
// $pattern = '/\<a.+href=[\'"]([^\'"]+)[\'"].+\>/';

$exists = preg_match($pattern, $html, $matches);
header('Content-Type: text/plain');
print_r($matches);
// var_dump($exists);
// +++a++a++a++++