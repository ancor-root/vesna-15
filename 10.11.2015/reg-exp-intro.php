<?php
/*
$pattern = '/ivan/';
$string = 'asd ivan asd';
*/

/*
$pattern = '/^ivan/';
$string = 'ivan asd';
*/

/*
$pattern = '/ivan$/';
// $string = 'asd ivan asd'; // false
$string = 'asd ivan'; // true
*/

/*
$pattern = '/^ivan$/';
// $string = 'ivan ivan'; // false
$string = 'ivan'; // true
*/

/*
$pattern = '/^(ivan){0,3}$/';
// $string = 'ivanivaniv anivanivanivan'; // false
// $string = 'ivanivanivanivanivanivan'; // true
$string = 'ivanivanivanivan'; // true
*/
/**
 * + - один и более
 * * - ноль и более
 * ? - один или ноль раз
 * {2,} - От 2 до бесконечности
 * {3} - 3 раза строго
 * {0,3} - От от 0 до 3 включая обе границы
 */

/*
$pattern = '/^i{0,3}van$/';
// $string = 'ivanivaniv anivanivanivan'; // false
// $string = 'ivanivanivanivanivanivan'; // true
$string = 'iiiiivan'; // true
*/

/*
$pattern = '/^[Ii]van$/';
// $pattern = '/^[asd]ivan$/';
// $string = 'ivanivaniv anivanivanivan'; // false
// $string = 'ivanivanivanivanivanivan'; // true
$string = 'ivan'; // true
*/

/*
$pattern = '/^[123]-[5678]$/';
// $pattern = '/^[asd]ivan$/';
// $string = 'ivanivaniv anivanivanivan'; // false
// $string = 'ivanivanivanivanivanivan'; // true
$string = '4-7'; // true
*/

/* // hex число
$pattern = '/^[0123456789abcdef]+$/';
// $pattern = '/^[asd]ivan$/';
$string = 'ff'; // true
*/

/*
$pattern = '/^[1-3]-[5-8]$/';
// $pattern = '/^[asd]ivan$/';
$string = '2-9'; // true
*/
/**
 * a-z
 * b-n
 * A-Z
 * A-F
 * 0-9
 * 4-8
 */
/*
$pattern = '/^[0-9a-fA-F]+$/';
// $string = '5z'; // false
// $string = '5a'; // true
// $string = 'ff'; // true
$string = 'FF'; // true
*/
// Пароль от 6 до 16 знаков, содержать только
// + буквы маленькие, англ
// + буквы большие, англ
// + любые цифры
// + тире
// + знак _
/*
$pattern = '/^[a-zA-F\\-0-9_]{6,16}$/';
// $string = '234'; // false
// $string = 'aaaa'; // false
// $string = 'aaaaaaaaaaaaaaaaa'; // false
// $string = 'Ялоаывоаыва'; // false
// $string = 'sdlfsds$dfsd'; // false
$string = 'a\sdf-sd343rf_'; // true
*/
/*
// $pattern = '/^((\+38)? \(056\) )?[1-9]\d{1,2}(-\d{2}){2}$/';
// $pattern = '/^((\+38)? \(056\) )?[1-9][0-9]{1,2}(-[0-9]{2}){2}$/';
$string = '+38 (056) 777-77-77';

$exists = preg_match($pattern, $string);
*/

/*
$pattern = '/ivan[^\d]/';
// $string = 'ivan*&^GH'; // true
$string = 'ivan7'; // false
*/
/*
// $pattern = '/^([ip][ve][at][rn])$/';
$pattern = '/^(ivan|petr)$/';
$string = 'ivtn'; // false
$string = 'ivan'; // true
*/

/*
$pattern = '/^I have .+ dogs$/';
$string = 'I have 56 dogs'; // false

$exists = preg_match($pattern, $string);
*/


var_dump($exists);