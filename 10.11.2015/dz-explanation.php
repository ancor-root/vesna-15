<?php

class Object
{
	public function __construct(array $properties)
	{
		foreach ($properties as $key => $value) {
			$this->$key = $value;
		}	
	}

	public function __set($name, $value)
	{
		$method = "set".ucfirst($name);
		if ( method_exists($this, $method) )
		{
			$this->{$method}($value);
		}
		else
		{
			die('error: try to set undefined property');
		}
	}

	public function __get($name)
	{
		$method = "get".ucfirst($name);

		if ( ! method_exists($this, $method) ) return null;

		return $this->$method();
	}
}



class Rect extends Object
{
	protected $width;
	public $height;

	public function getArea()
	{
		return $this->width * $this->height;
	}
}

class Paralelepiped extends Rect
{
	public $length;

	public function getValume()
	{
		return $this->getArea() * $this->length;
	}
}


// $rect1 = new Rect(10, 51);
// 
// $rect1->width = 50;
// $rect1->height = 51;
// 
$rect1 = new Rect([
	'width'  => 50,
	'height' => 51,
]);

// $paralelepiped1 = new Paralelepiped(10, 10, 5);
$paralelepiped1 = new Paralelepiped([
	'width'  => 10,
	'height' => 10,
	'length' => 51,
]);

echo 'area:' . $rect1->area;
echo "<br>";
echo 'valume:' . $paralelepiped1->valume;