<?php
/*
// Введение в foreach
$user = [
  'email'    => 'ivan@microsoft.com',
  'age'      => '35',
  'pwd'      => '212ed34df23r2',
  'city'     => 'Dnipropetrovsk',
  'carColor' => 'red',
];

// for ($i = 0; $i < 5; $i++) {
//   echo $user[ $i ]; // Работать не будет
// }

// foreach ( $user as $item )
foreach ( $user as $key => $item )
{
  echo "item:$key ------- $item<br>";
}*/


$menu = [
  '//www.google.ru' => ['Google', 'Ссылка на гугл'],
  '//www.mail.ru'   => ['Mail ru', 'Ссылка на mail ru'],
];

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Menu foreach</title>
</head>
<body>
  <nav>
    <?php foreach($menu as $link => $item): ?>
    <a href="<?= $link ?>"><?= $item ?></a>
    <?php endforeach ?>
  </nav>
</body>
</html>