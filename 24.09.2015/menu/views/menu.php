<?php /*
Шаблон вывода меню
==================
@var array $menu основное меню сайта, пример: [  ['link' => 'google.ru', 'name' => 'Гугл']  ] 
*/ ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>My first menu</title>
</head>
<body>
  <h1>Мой сайт</h1>
  <nav>
    <?php foreach($menu as $key => $item): ?>
    <a title="<?= $item['title'] ?>" href="<?= $item['link'] ?>"><?= $item['name'] ?></a>
    <?php endforeach ?>
  </nav>
  <hr>
  <?=$content?>
  <hr>
  &copy Все права защищены
</body>
</html>