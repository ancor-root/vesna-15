<?php
// НИКАКОГО ВЫВОДА!!!

$menu = [
  [ 'link' => 'google.ru', 'name' => 'гугл',],
  [ 'link' => 'yandex.ru', 'name' => 'yandex' ],
  [ 'link' => 'main.ru', 'name'   => 'mail' ],
  [ 'link' => 'vk.ru', 'name'     => 'vk' ],
];

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Menu</title>
</head>
<body>
<!-- 
for(): - endfor
while(): - endwhile
if(): endif
if(): .... else: ..... endif
if(): .... else: ..... elseif: .... endif
foreach(): endforeach

<?php echo $title; ?>
<?= $title ?>
-->
    <ul>
      <?php for ($i = 0, $len = count($menu); $i < $len; $i++): ?>
      <li><a href="<?= $menu[$i]['link'] ?>"><?= $menu[$i]['name'] ?></a></li>
      <?php endfor ?>
    </ul>
</body>
</html>