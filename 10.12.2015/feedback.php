<?php
	$dsn = 'mysql:dbname=vesna_15;host=127.0.0.1';
	$user = 'root';
	$password = '111';

	try {
	    $dbh = new PDO ($dsn, $user, $password);
	} catch (PDOException $e) {
	    echo 'Connection failed: ' . $e->getMessage();
	}

	if ( ! empty($_POST)) {
		$email = isset($_POST['email']) ? $_POST['email'] : null;
		$name  = isset($_POST['name'])  ? $_POST['name']  : null;
		$text  = isset($_POST['text'])  ? $_POST['text']  : null;

		$email = $dbh->quote($email);
		$name  = $dbh->quote($name);
		$text  = $dbh->quote($text);

		$sql = "INSERT INTO `feedback` SET `email` = $email";
		// echo "SQL:$sql:";
		// die;
		$dbh->exec($sql);
		header('Location: feedback.php');
		die;
		// $email = $_POST['email'] ?? : null; // php7
	}

	$messages = $dbh->query("SELECT `id`, `email`, `name`, `text` FROM `feedback`");

	// $messages = [
	// 	[
	// 		'name' => 'Deja',
	// 		'email' => 'bubinga@tavers.net',
	// 		'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus unde obcaecati mollitia consequuntur sapiente corrupti modi illo voluptatem delectus esse assumenda saepe accusantium, dolorum a quas, accusamus cum aliquam. Sequi minus error saepe, perferendis quasi, dignissimos perspiciatis. Accusantium voluptas, quam, vero quod quos aperiam aliquam temporibus enim mollitia. Odio, doloribus.',
	// 	],
	// 	[
	// 		'name' => 'Berry',
	// 		'email' => 'yeasting@alimentation.co.uk',
	// 		'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil minus eveniet eligendi consequatur at, ipsa enim voluptatibus eos ut, temporibus, delectus. Hic fuga quas molestias dolor deserunt inventore provident recusandae minus asperiores aut officiis, ab repellat voluptatum ex nam perspiciatis impedit architecto officia a ratione animi aliquid dolores? Deserunt, libero.',
	// 	],
	// ];
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	
<div class="container">
	
	<div class="new-message">
		<form action="" method="POST">
			<p>email: <input type="text" name="email"></p>
			<p>name: <input type="text" name="name"></p>
			<p>your message: <textarea name="text" id="" cols="30" rows="10"></textarea></p>

			<p><button type="submit">Send</button></p>
		</form>
	</div>
	<hr>
	<div class="messages">

		<?php foreach($messages as $one): ?>
		<div class="message" style="border-bottom: 1px #123 dashed">
			<h5><span class="email"><?=($one['email'])?></span> <span class="name"><?=($one['name'])?></span></h5>
			<div class="text"><?=($one['text'])?></div>
		</div>
		<?php endforeach ?>

	</div>

</div>

</body>
</html>