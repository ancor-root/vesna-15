<?php
// Этот файл - точка входа, пока что он же контроллер
require ('lib.php');

// $content = 'Основная часть сайта';
// $sidebar = 'Боковая панель';


$articles = include ('models/article.php');

$widgetArticlesTitles = render('widgets/article-titles', [
  'articles' => $articles,
]);

$widgets = [
  [
    'title' => 'Соц сети',
    'content' => 'vk.com<br>facebook.com'
  ],
  [
    'title' => 'Недавно просмотренные статьи',
    'content' => 'Статья о собаках'
  ],
  [
    'title' => 'Недавно просмотренные статьи',
    'content' => $widgetArticlesTitles
  ]
];

// Подключаем шаблон статей
$content = render('article/index', [
  'articles' => $articles,
]);

// Подключение боковой панели
$sidebar = render('layouts/sidebar-1', [
  'widgets' => $widgets
]);

$page = render('layouts/base', [
  'sidebar' => $sidebar,
  'content' => $content,
]);

echo $page;
// include ('views/layouts/base.php');