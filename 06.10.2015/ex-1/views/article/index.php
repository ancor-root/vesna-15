<!-- div.article>h3+div.article-anonce+.article-footer>.date -->
<? foreach($articles as $key => $article): ?>

  <div class="article">
    <h3><?=$article['title']?> <span class="article-date">(<?=$article['created_at']?>)</span></h3>
    <div class="article-anonce"><?=$article['anonce']?></div>
    <div class="article-footer">
      <div class="article-author">Автор: <?=$article['author']?></div>
    </div>
    <hr>
  </div>
  
<? endforeach ?>
