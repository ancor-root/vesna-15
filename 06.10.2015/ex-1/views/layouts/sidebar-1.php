<? foreach($widgets as $key => $widget): ?>
  <div class="widget">
    <h3 class="widget-title"><?= $widget['title'] ?></h3>
    <div class="widget-content"><?= $widget['content'] ?></div>
    <hr>
  </div>
<? endforeach ?>
