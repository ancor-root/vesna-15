<?php
/**
 * Основной шаблон
 * ===============
 * @var  string $content основное содержимное сайта в виде html строки
 * @var  string $sidebar боковая панель в виде куска html кода
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <link rel="stylesheet" href="media/site.css">
</head>
<body>
  <h2>Мой первый блог</h2>
  <hr>
  <div class="main-container">
    <div class="main-content">
      <?= $content ?>
    </div>
    <div class="sidebar-right">
      <?= $sidebar ?>
    </div>
  </div>
  <hr>
  &copy; Все права защищены
</body>
</html>