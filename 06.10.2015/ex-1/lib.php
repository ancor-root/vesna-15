<?php
/**
 * Отрендерить шаблон
 * @param  string $template Название шаблона
 * @param  array  $params   
 * @return string           Готовый шаблон
 * @example : пример параметров
 * $params = [
 *   'title'   => 'Это заголовок',
 *   'content' => '<div>......'
 * ];
 */
function render($template, $params = [])
{
  foreach ($params as $key => $value) {
    $$key = $value;
  }

  ob_start(); // Запустить буферизацию
  include ("views/{$template}.php");
  $result = ob_get_clean(); // Очистить буфер и получить его значение

  return $result;
}