<?php
$from = 'ex-1';
$to   = 'ex-1-copy';

// для того что бы браузер отобразил страницу в виде text'a а не в виде html
header('Content-Type: text/plain');

// Запускаем копирование
$copyResult = copyDir($from, $to);

// Результат копирования
var_dump($copyResult);

/**
 * Рекурсивно копировать папку со всем ее содержимым
 * @param  string  $from исходный путь
 * @param  string  $to   Целевой путь
 * @return boolean       Удалось ли скопировать
 */
function copyDir($from, $to)
{
  // Не существует исходный путь или уже существует целевой
  if ( ! file_exists($from) || file_exists($to) ) return false;

  if ( is_dir($from) ) // Копируем папку
  {
    echo "dir  : $from -> $to\n";
    mkdir($to); // Создание целевой папки
    $content = scandir($from);
    foreach ($content as $key => $itemName) {

      // Пропуск стандартных директорий
      if ( $itemName == '.' || $itemName == '..' ) continue;

      $result = copyDir( "$from/$itemName", "$to/$itemName" ); // Рекурсивное копирование содержимого
      if ( ! $result ) return false;
    }
  }
  else // Копируем файл
  {
    echo "file : $from -> $to\n";
    $fileContent = file_get_contents($from);
    file_put_contents($to, $fileContent);
  }

  return true; // Все успешно
}