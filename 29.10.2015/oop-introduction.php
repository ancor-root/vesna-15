<?php
// 1. Имя класса с большой буквы(не объекта)
// Свойство класса - переменная внутри класса
// Метод класса - ф-я объявленная внутри класса

/*
// Пример свойства
class Book
{
	public $name;
}

$book1 = new Book;
$book1->name = 'my book 1';

$book2 = new Book;
$book2->name = 'my book 2';

echo 'name of book 1:' . $book1->name.'<br>';
echo 'name of book 2:' . $book2->name.'<br>';*/

/*
// Пример метода
class Book
{
	public $name;

	public function print_me()
	{
		echo 'name of book:' . $this->name . '<br>';
	}
}

$book1 = new Book;
$book1->name = 'my book 1';

$book2 = new Book;
$book2->name = 'my book 2';

$book1->print_me();
$book2->print_me();
*/

class Book
{
	public $name;

	public function print_me()
	{
		echo 'name of book:' . $this->name . '<br>';
	}

	public function __construct($bookName)
	{
		$this->name = $bookName;
		// echo 'constructed<br>';
	}
}

// echo 'before create object<br>';
$book1 = new Book('my book 1');
// echo 'after create object<br>';
// $book1->name = 'my book 1';
// echo 'after property is iset<br>';
$book2 = new Book('my book 2');
/*
$book2 = new Book;
$book2->name = 'my book 2';
*/
$book1->print_me();
$book2->print_me();