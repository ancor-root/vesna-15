<?php
// help : http://php.net/manual/ru/function.file-get-contents.php

// Создать новый файл или перезаписать существующий
// file_put_contents('filename.txt', 'some data');

// Создать новый файл или перезаписать существующий
// file_put_contents('filename.txt', 'some data', FILE_APPEND);

// Считать файл
// $fileStr = file_get_contents('filename.txt');
// echo $fileStr;

// Проверить существование файла - file_exists
// if ( file_exists('filename.txt') )
// {
//   echo file_get_contents('filename.txt');
// }
// else
// {
//   echo 'Файл не найден';
// }

// Удалить файл
// unlink('filename.txt');





// =======================================================================
// Директории ==============================================================
// =======================================================================

// Просканировать папку и получить ее содержимое
// $directories = scandir('C:/xampp');
// print_r($directories);

// Создать папку
// mkdir('folderName');

// Создать папку с большой вложенностью
// mkdir('folderName/1/1/1/1', 0777, true);

// Удалить папку(только один уровень вложенности)
// rmdir('folderName');

// Проверить "папка ли это" is_dir
// if ( is_dir('./filename.txt') )
// {
//   echo 'dir';
// }
// else
// {
//   echo 'it is not dir';
// }




// =======================================================================
// системные константы ==============================================================
// =======================================================================


// echo '__DIR__: ' .__DIR__ .'<br>';
// echo '__FILE__: '.__FILE__.'<br>';
// echo '__LINE__: '.__LINE__.'<br>';

// Разделитель директорий
// 'folder1' . DIRECTORY_SEPARATOR . 'folder2'
// echo DIRECTORY_SEPARATOR;
// /
// \

// Родительская папка
// echo __DIR__ . '<br>';
// echo dirname(__DIR__) . '<br>';
// echo dirname(dirname(__DIR__)) . '<br>';

// Сконвертировать относительные пути в абсолютные 
// $path = __DIR__ . '/../../22.09.2015';
// echo $path . '<br>';
// echo realpath($path);