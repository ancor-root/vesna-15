<?php
  $sended = false;
  if ( isset($_POST['login-form']) )
  {
    $sended = true;
    $login    = $_POST['login'];
    $password = $_POST['password'];
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>form 1</title>
</head>
<body>
  <?php if( $sended ): ?>
    <p><?= $login ?></p>
    <p><?= $password ?></p>
  <?php endif ?>

  <form action="" method="post">
    <p>login: <input type="text" name="login"></p>
    <p>password: <input type="text" name="password"></p>
    <p><button type="submit" name="login-form">Отправить</button></p>
  </form>
</body>
</html>