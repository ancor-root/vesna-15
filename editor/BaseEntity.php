<?php
include "StringHelper.php";

/**
 * Класс для работы с сущностями
 */
class Entity
{
	public function getName()
	{
		$className = get_class();
		echo StringHelper::camel2id($className);
	}
}