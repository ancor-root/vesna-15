var messageTpl = '\
<div class="message">\
  <div class="message-header">\
    <strong class="author">{{author}}</strong>\
  </div>\
  <div class="message-body">{{text}}</div>\
</div>\
';

function render(tpl, vars)
{
  // for ( var name in vars ) {}
  // for ( var i = 0; i < keys.length; keys++)
  // {
    // vars[i]
  // }

  Object.keys(vars).forEach(function(name) {
    // console.log('-------------');
    // console.log('{{' + name + '}}', "\n");
    // console.log(vars[name], "\n");
    // console.log(tpl, "\n");

    tpl = tpl.replace('{{' + name + '}}', vars[name]);

    // console.log(tpl);

    // console.log('------------- END');
  });

  return tpl;
}

$('form[name=new]').submit(function (event) {
  event.preventDefault();

  var messageText   = $(this).find('[name=message-text]').val();
  var messageAuthor = $(this).find('[name=message-author]').val();
  console.log(messageText/*, messageAuthor*/);

  var data = {
    action : 'new-message',
    data   : {
      text   : messageText,
      author : messageAuthor
    },
  };

  $.post('chat.php', data, function(response) {

    console.log('response:', response);

    var newMessage = render(messageTpl, {
      author : messageAuthor,
      text   : messageText,
    });

    $('.messages').append(newMessage);

  });
});

// {
//   action : '',
//   data : ....
// }