<?php
// Подключаем файл конфигурации
$config = require('config.php');

// Подключение основной библиотеки
require('lib/lib.php');

// index.php?controller=index&action=index
// index.php/index/index
// /index/index

$controller = isset($_GET['controller']) ? $_GET['controller'] : 'index';
$action     = isset($_GET['action'])     ? $_GET['action']     : 'index';

require("${config['root']}/controllers/$controller.php");

$controller = ucfirst($controller); // Сделать первую букву заглавной
$controllerName = "controller${controller}";

// Вызов ф-и имя которой записано в переменной. То есть вызов контроллера
$page = $controllerName($action);
echo $page;

// $funcName = 'my_function';
// $func = function() { ... };