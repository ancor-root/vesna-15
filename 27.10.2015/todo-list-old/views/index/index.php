<?php /*
Шаблон основной страницы
========================
@var $list - список дел [ [0/1, 'task-name'] ]
*/ ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?=$title?></title>
  <script type="text/javascript" src="./media/jquery-2.1.4.min.js"></script>
  <script type="text/javascript" src="./media/main.js"></script>
</head>
<body>

<table>
  <thead>
    <tr>
      <th>#</th>
      <th>Название</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <!-- form>p>input[name=name]+button:s{Создать} -->
    <form action="index.php?action=create" method="POST" >
      <p><input type="text" name='name'><button type="submit" name="create-task">Создать</button></p>
    </form>

    <? foreach($list as $key => $task): ?>

      <tr>
        <td><?=$key?></td>
        <td<?= ($task[0]) ? ' style="color: #aaa;"' : '' ?>><?=$task[1]?></td>
        <td>
          <a class="delete-btn" href="index.php?action=delete&id=<?=$key?>">[delete]</a>
          <a class="done-btn" href="index.php?action=done&id=<?=$key?>">[done]</a>
        </td>
      </tr>
      
    <? endforeach ?>

  </tbody>
</table>

</body>
</html>