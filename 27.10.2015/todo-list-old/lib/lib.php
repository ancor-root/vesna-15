<?php

/**
 * Отрендерить шаблон
 * @param  string $template Название шаблона
 * @param  array  $params   
 * @return string           Готовый шаблон
 * @example : пример параметров
 * $params = [
 *   'title'   => 'Это заголовок',
 *   'content' => '<div>......'
 * ];
 * @example : пример использования
 * $page_str = render('views/index', [ 'title' => 'Название страницы']);
 */
function render($template, $params = [])
{
  global $config;

  foreach ($params as $key => $value) {
    $$key = $value;
  }

  ob_start(); // Запустить буферизацию
  include ("${config['root']}/views/{$template}.php");
  $result = ob_get_clean(); // Очистить буфер и получить его значение

  return $result;
}
