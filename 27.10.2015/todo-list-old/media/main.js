$(function() {

	$('.delete-btn').click(function(event) {
		event.preventDefault();
		
		var $a = $(this);

		var href = $(this).attr('href');
		var params = href.split('?')[1].split('&');

		var id;
		params.forEach(function(item) {
			// console.log(item);
			var tmp = item.split('=');
			if ( tmp[0] == 'id' ) id = tmp[1];
		});

		// $.post('index.php?controller=index&action=deleteAjax&id='+id, );
		$.post('index.php?controller=index&action=deleteAjax', {
			id : id,
		}, function(response) {
			// .parent().parent();
			// $(this).closest('tr');
			if ( response.status === true )
			{
				$a.closest('tr').remove();
			}
		});
		// console.log(href);

	});

});