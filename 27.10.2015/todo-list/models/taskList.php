<?php
function tasksLoad()
{
  global $config;
  static $tasks;
  if ( $tasks === null )
  {
    $tasksStr = file_get_contents("${config['root']}/data/${config['db']}");
    $tasks = unserialize($tasksStr);
  }

  return $tasks;
}

function tasksSave(array $tasks)
{
  global $config;
  $tasksStr = serialize($tasks);
  file_put_contents("${config['root']}/data/${config['db']}", $tasksStr);
}

function taskCreate($name)
{
  $tasks = tasksLoad();
  $tasks[] = [0, $name];
  tasksSave($tasks);
}

function taskDelete($id)
{
  $tasks = tasksLoad();
  unset($tasks[$id]);
  tasksSave($tasks);
}

function taskDone($id)
{
  $tasks = tasksLoad();
  if ( $tasks[$id][0] == 0 )
  {
    $isDone = true;
    $tasks[$id][0] = 1;
  }
  else
  {
    $isDone = false;
    $tasks[$id][0] = 0;
  }
  tasksSave($tasks);

  return $isDone;
}

function taskUpdateName($id, $newName)
{
  $tasks = tasksLoad();
  $tasks[$id][1] = $newName;
  tasksSave($tasks);
}