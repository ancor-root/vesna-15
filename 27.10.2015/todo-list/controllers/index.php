<?php
require_once("${config['root']}/models/taskList.php");
// commentForArticle.php : controllerCommentForArticle()
// comment-for-article => commentForArticle

// function controllerIndex($actionName = 'index')
function controllerIndex($actionName)
{
  $baseTplName = 'index';

  $actionIndex = function () use ($baseTplName) {
    $tasks = tasksLoad();

    return render("$baseTplName/index", [
      'list' => $tasks,
    ]);
  };

  $actionCreate = function() {
    if ( isset($_POST['create-task']) )
    {
      // $updated = true;
      $name = isset($_POST['name']) ? $_POST['name'] : null;
      taskCreate($name);

      header('Location: index.php');
      die;
    }

    echo 'error';
  };

  $actionDelete = function() {
    $id = isset($_GET['id']) ? $_GET['id'] : null;
    taskDelete($id);

    header('Location: index.php');
    die;
  };

  $actionDoneAjax = function() {
    if ( $_SERVER['REQUEST_METHOD'] == 'POST' )
    {
      $id = isset($_POST['id']) ? $_POST['id'] : null;
      $isDone = taskDone($id);

      header('Content-Type: text/json');
      echo json_encode([
        'done' => $isDone
      ]);
    }
  };

  $actionUpdateName = function() {
    if ( $_SERVER['REQUEST_METHOD'] == 'POST' )
    {
      $id   = isset($_POST['id'])   ? $_POST['id']   : null;
      $name = isset($_POST['name']) ? $_POST['name'] : null;
      taskUpdateName($id, $name);

      header('Content-Type: text/json');
      echo json_encode([
        'status' => true
      ]);
    }
  };

  $actionTest = function () {
    // render('index/test');
    
    
    // return 'Created';
  };

  $actionName = "action" . ucfirst($actionName); // actionIndex
  // благодаря двойному $$ - $actionIndex();
  $result = $$actionName();
  return $result;
} // end controllerIndex