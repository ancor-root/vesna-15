$('input[name=done]').click(function(event) {

  // Перед отправкой формы нужно записать id задачи в input с name=id 
  var $checkbox = $(this);
  var $tr = $checkbox.closest('tr');

  var id = $(this).attr('data-id'); // Считываем атрибут data-id
  $('input[name=id]').val(id); // 

 	console.log('test', id);
	$.post('index.php?controller=index&action=doneAjax', {
		id : id,
	}, function(response) {
		if ( response.done === true )
		{
			$checkbox.prop('checked', true);
			$tr.addClass('done');
			console.log( 'поставить галочку' );
		}
		else
		{
			$checkbox.prop('checked', false);
			$tr.removeClass('done');
			console.log( 'снять галочку' );
		}
	});
});

// form[name=tasks] 
$('table tr td:nth-child(3)').dblclick(function() {
	
	var oldText = $(this).html();

	$(this).html('<input value="'+oldText+'">');
	var $td = $(this);
	var id  = $td.parent().attr('data-id');

	var $input = $(this).find('input');
	$input.keypress(function(event) {
		if ( event.which !== 13 ) return;
		// console.log('pressed', event.which);

		var newValue = $input.val();
		$.post('index.php?controller=index&action=updateName', {
			name : newValue,
			id   : id,
		}, function(response) {
			if ( response.status === true )
			{
				$td.html( newValue );
			}
			else
			{
				console.error( response );
			}
		});
	});

});


	// `some text ${oldText}`
	// console.log( $(this).html() );