<?php

// my_pow(2, 5); // 32

// 2^5 = 2 * 2^4 (  return 2 * my_pow(2, 4)   )
// 2^4 = 2 * 2^3
// 2^3 = 2 * 2^2
// 2^2 = 2 * 2^1

// function my_pow($n, $pow)
// {
//   $result = 1;
//   for($i = 0; $i < $pow; $i++)
//   {
//     // $result *= $n;
//     $result = $result * $n;
//   }
//   return $result;
// }

function my_pow($n, $pow)
{
  if ( $pow == 1 ) return $n;
  if ( $pow == 0 ) return 1;

  return $n * my_pow($n, $pow-1);
}

echo pow(2, 5); // стандартная ф-я
echo '<br>';
echo my_pow(2, 5);


/*
  Определить по номеру элемента его значение
  my_fib($n);
  
  0,1,1,2,3,5......
*/