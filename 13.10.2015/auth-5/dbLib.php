<?php
define('FILE_DB', 'db.txt');

$users = null;

function dbLoad()
{
  if ( ! file_exists(FILE_DB) ) return false;
  $users_str = file_get_contents(FILE_DB);

  $users = unserialize($users_str);
  if ( $users === false ) return false;

  $GLOBALS['users'] = $users;
  return true;
}

function dbSave()
{
  $users = $GLOBALS['users'];
  $users_str = serialize($users);

  $bytes = file_put_contents(FILE_DB, $users_str);
  if ( $bytes === false ) return false;

  return true;
}

function getUserByLogin($login)
{
  $users = $GLOBALS['users'];

  foreach ($users as $key => $user) {
    if ( $login == $user['login'] ) return $user;
  }

  return null;
}

function getUserByUid($uid)
{
  $users = $GLOBALS['users'];
  
  foreach ($users as $user) {

    $hash = md5( $user['login'] . $user['pwd'] );
    if ( $uid == $hash ) return $user;
  }

  return null;
}