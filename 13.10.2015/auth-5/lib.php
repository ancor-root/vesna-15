<?php
require('dbLib.php');



function get_user()
{
  $uid = isset($_COOKIE['UID']) ? $_COOKIE['UID'] : null;

  return getUserByUid($uid);
}

function login($login, $pwd)
{
  $user = getUserByLogin($login);
  if ( ! $user ||  $pwd != $user['pwd']  ) return false;

  $hash = md5( $login . $pwd );
  setcookie('UID', $hash);

  return true;
}

function logout()
{
  setcookie('UID', '', time()-60*60*24 );
}

function reg($login, $pwd, $name)
{
  $GLOBALS['users'][] = [
    'login' => $login,
    'pwd'   => $pwd,
    'name'  => $name,
  ];

  return dbSave();
}