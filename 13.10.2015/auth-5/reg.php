<?php
require('lib.php');
dbLoad();

$message = null;

if ( $_SERVER['REQUEST_METHOD'] == 'POST' )
{
  $login = isset($_POST['login']) ? $_POST['login'] : null;
  $pwd   = isset($_POST['pwd'])   ? $_POST['pwd']   : null;
  $name  = isset($_POST['name'])  ? $_POST['name']   : null;

  if ( reg($login, $pwd, $name) ) 
  {
    header('Location: auth.php');
    die;
  }
  else
  {
    $message = 'Incorrect data';
  }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>reg example</title>
</head>
<body>
  <div class="messages"><?=$message?></div>

  <!-- form[method=POST]>p>{login}+input[name=login]^+p>{password}+input[name=pwd]^+p>button:s{Войти} -->
  <form action="" method="POST">
    <p>login<input type="text" name="login"></p>
    <p>password<input type="text" name="pwd"></p>
    <p>name<input type="text" name="name"></p>
    <p><button type="submit">Зарегистрироваться</button></p>
  </form>
</body>
</html>