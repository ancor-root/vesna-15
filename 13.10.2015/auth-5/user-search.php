<?php
require('lib.php');
dbLoad();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Authorization example</title>
</head>
<body>

  <table>
    <tr>
      <th>#</th>
      <th>Name</th>
    </tr>

    <? $i = 1; foreach($users as $user): ?>
      <tr>
        <td><?= $i++ ?></td>
        <td><?=$user['name']?></td>
      </tr>
    <? endforeach ?>

  </table>
</body>
</html>