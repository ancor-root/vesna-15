<?php

// $res = unserialize('');
// var_dump($res);

// die;

$file_str = file_get_contents('tasks.db');
$list = unserialize($file_str);
if ( gettype($list) != 'array' ) $list = [];

$updated = false;
if ( isset($_POST['create-task']) )
{
  $updated = true;
  $name = isset($_POST['name']) ? $_POST['name'] : null;
  $list[] = [0, $name];
}
if ( /* передан ли в $_GET элемент delete */ )
{
  /*
  поставить updated в true
  $n = isset($_GET[...]) ? : ;
  Удалить из $list элемент с переданным номером - $, ф-я unset
   */
}
if ( /* передан ли в $_GET элемент done */ )
{
  /*
  поставить updated в true
  $index = isset($_GET[...]) ? ... : ... ;
  в массиве $list обращаемся к элементу $index и меняем в нем нулевой элемент на 1
  */
}

if ( $updated )
{
  $file_str = serialize($list);
  file_put_contents('tasks.db', $file_str);
}


// $list = [
//   [0, 'Задача 1'],
//   [1, 'Задача 2'],
// ];



?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>to do list - v1</title>
</head>
<body>

<table>
  <thead>
    <tr>
      <th>#</th>
      <th>Название</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <!-- form>p>input[name=name]+button:s{Создать} -->
    <form action="" method="POST" >
      <p><input type="text" name='name'><button type="submit" name="create-task">Создать</button></p>
    </form>

    <? foreach($list as $key => $task): ?>

      <tr>
        <td><?=$key?></td>
        <td<?= ($task[0]) ? ' style="color: #aaa;"' : '' ?>><?=$task[1]?></td>
        <td>
          <a href="todo-list-v1.php?delete=<?=$key?>">[delete]</a>
          <a href="todo-list-v1.php?done=<?=$key?>">[done]</a>
        </td>
      </tr>
      
    <? endforeach ?>

  </tbody>
</table>

</body>
</html>