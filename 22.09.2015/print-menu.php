<?php
$menu = [
  [ 'link' => 'google.ru', 'name' => 'гугл',],
  [ 'link' => 'yandex.ru', 'name' => 'yandex' ],
  [ 'link' => 'main.ru', 'name'   => 'mail' ],
  [ 'link' => 'vk.ru', 'name'     => 'vk' ],
];

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Menu</title>
</head>
<body>
    <ul>
      <?php // for($i = 0; $i < count($menu); $i++) { // плохой вариант, множественный вызов count ?>
      <?php for($i = 0, $len = count($menu); $i < $len; $i++) { ?>
      <li><a href="<?php echo $menu[$i]['link'] ?>"><?php echo $menu[$i]['name'] ?></a></li>
      <?php } ?>
    </ul>
</body>
</html>