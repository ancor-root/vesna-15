<?php
// Массивы
// $arr = array();

// Объявление
// $arr = []; // 5.4


// Объявление со значениями
// $arr = [1,2,3,4,5,6];
// $arr = ['ivan', 'kate'];
// $arr = [true, true, false];

// $arr = [1, false, 'kate']; // смешанные

// Вывод массива
// echo $arr; // нельзя
// print_r($arr);
// var_dump($arr); // Более подробно

// получить конкретные элементы
// echo $arr[2];


// Проблема числовых массивов
// $user = [
//   'ivan@microsoft.com',
//   '35',
//   '212ed34df23r2',
//   'Dnipropetrovsk',
//   'red',
// ];


// echo $user[3];

/*
// Ассоциативные массивы
$user = [
  'email'    => 'ivan@microsoft.com',
  'age'      => '35',
  'pwd'      => '212ed34df23r2',
  'city'     => 'Dnipropetrovsk',
  'carColor' => 'red',
];

// print_r($user);
echo $user['city'];

*/

// Смешанный массив
/*$user = [
  99         => 'hello',
  'email'    => 'ivan@microsoft.com',
  'age'      => '35',
  88         => 'qwe',
  'pwd'      => '212ed34df23r2',
  'city'     => 'Dnipropetrovsk',
  'carColor' => 'red',
  '180',
];

print_r($user);*/
// echo $user['city'];


// Добавление элемента
/*$numbers = [1,2,3,4];
print_r($numbers);
$numbers[] = 8;

print_r($numbers);
*/
// Добавление элемента 2
/*$numbers = [1,2,3,4];

print_r($numbers);


$numbers[88] = 5;
$numbers[] = 6;
$numbers[33] = 7;
$numbers[] = 8;
*/


// Плохой тон создания массивов
/*
$arr = [ 3 => 999 ]; // Хорошо
$arr[3] = 999; // Плохо
print_r($arr);
*/



// Двухмерные массивы
/*

$users = [];

$users[] = [
  'name'  => 'ivan',
  'email' => 'ivan@microsoft.com',
  'age'   => 25,
];
$users[] = [
  'name'  => 'petro',
  // 'email' => 'ivan@microsoft.com',
  'age'   => 25,
];
$users[] = [
  // 'name'  => 'kate',
  'email' => 'ivan@microsoft.com',
  'age'   => 25,
];

$users[] = 1;
$users[] = false;
$users[] = 'asdasdasd';

print_r($users);
*/

// Передача по ссылке(массивы всегда передаются по значению)
/*$arr = [1,2,3];
$arr2 = &$arr;
print_r($arr);
print_r($arr2);

$arr2[0] = 888;
$arr[2] = 9999;


print_r($arr);
print_r($arr2);
*/


// Передача массива в ф-ю
/*
$user = [
  'name'  => 'ivan',
  'email' => 'ivan@microsoft.com',
  'age'   => 25,
];
$user2 = [
  'name'  => 'kate',
  'email' => 'kate@microsoft.com',
  'age'   => 25,
];

printUser($user);
printUser($user2);

function printUser($user)
{
  echo "<strong>name:</strong> {$user['name']}<br>";
  echo "<strong>email:</strong> {$user['email']}<br>";
  echo "<strong>age:</strong> {$user['age']}<br>";
  echo "<hr>";
}
*/


// Возвращение массива
/*$myRange = getRange(100, 150);
// var_dump($myRange);
print_r($myRange);

function getRange($from, $to)
{
  $range = [];
  for($i = $from; $i < $to; $i++) $range[$i] = '';
  return $range;
}*/