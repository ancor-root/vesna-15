<?php
echo '<pre>';
triangle(10);
echo '</pre>';

// 1. Доделать аргумент $filled = true,
// 2. $char = ' ' - символ которым залить
function triangle($side)
{
  for($h = 0; $h < $side; $h++)
  {
    for($w = 0; $w < $h+1; $w++)
    {
      $isEmpty = ($h > 1 && $h < $side-1) && ($w > 0 && $w < $h) ;
      echo $isEmpty ? '#' : '*';
    }
    echo "\n";
  }
}