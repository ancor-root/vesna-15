<?php
echo myMax(5,3); // 5
echo myMax(5,9); // 9

function myMax(..., ...)
{
  return ...;
}

echo myMaxArr([3,5,78,2,5,72]); // 78

function myMaxArr(array $numbers)
{
  for (....)
  {
    if ( ... ) return ...
  }
}