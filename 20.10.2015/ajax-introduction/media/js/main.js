$('form').submit(function(event) {
  event.preventDefault();
  
  var
      n1 = $('form[name=calc] input[name=n1]').val(),
      n2 = $('form[name=calc] input[name=n2]').val(),
      operation = $('form[name=calc] [name=operation]').val();

  $.post('ajax.php', {
    n1 : n1,
    n2 : n2,
    operation : operation,
  }, function(response) {
    console.log(response);
    $('form[name=calc] input[name=result]').val(response.result);
  });

});