<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
</head>
<body>

  <form method="GET" action="ajax.php" name="calc">

    <input type="text" name="n1">

    <select name="operation" id="operation">
      <option value="plus">+</option>
      <option value="minus">-</option>
    </select>

    <input type="text" name="n2">
    <button type="submit">=</button>

    <input type="text" name="result" readonly="true">

  </form>

  <script src="media/js/jquery-2.1.4.min.js"></script>
  <script src="media/js/main.js"></script>
</body>
</html>