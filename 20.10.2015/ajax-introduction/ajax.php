<?php

$n1 = isset($_POST['n1']) ? $_POST['n1'] : null;
$n2 = isset($_POST['n2']) ? $_POST['n2'] : null;
$operation = isset($_POST['operation']) ? $_POST['operation'] : null;

$response = [];

if ( $operation == 'plus' )
{
  $response['result'] = $n1 + $n2;
}
else if ( $operation == 'minus' )
{
  $response['result'] = $n1 - $n2;
}

header('Content-Type: text/json');
echo json_encode($response); // Закодировать в json






// $n1 = isset($_GET['n1']) ? ....
// $n2 ...
// echo $n1 + $n2;

// header('Content-Type: text/json');
// $response = [
//   'login' => "Hello, {$_POST['login']}",
// ];
// echo json_encode($response); // Закодировать в json

// echo "{ \"login\" : \"Hello, {$_POST['login']}\" }";

// echo "Hello, {$_POST['login']}!";
// XML - устарел
// JSON - Java Script Object Notation
// 
// x-www-form-urlencode
// multipart/form-data

// {
//   'login' : 'petro'
// }