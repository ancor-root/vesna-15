<?php /*
Шаблон основной страницы
========================
@var $list - список дел [ [0/1, 'task-name'] ]
*/ ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?=$title?></title>
</head>
<body>

<form action="index.php?action=create" method="POST" >
  <p><input type="text" name='name'><button type="submit" name="create-task">Создать</button></p>
</form>

<form method="POST" name="tasks" action="index.php?action=done">
  <table>
    <thead>
      <tr>
        <th>#</th>
        <th>Название</th>
        <th></th>
      </tr>
    </thead>
    <tbody>

      <? foreach($list as $key => $task): ?>

        <tr>
          <td><?=$key?></td>
          <td><input data-id="<?=$key?>" type="checkbox" value="ok" name="done"<?= ($task[0]) ? ' checked="true"' : '' ?>></td>
          <td<?= ($task[0]) ? ' style="color: #aaa;"' : '' ?>><?=$task[1]?></td>
          <td>
            <a href="index.php?action=delete&id=<?=$key?>">[delete]</a>
            <!-- <a href="index.php?action=done&id=<?=$key?>">[done]</a> -->
          </td>
        </tr>
        
      <? endforeach ?>

      </tbody>
    </table>
    <input type="hidden" name="id" value="">
</form>

  <script type="text/javascript" src="media/js/jquery-2.1.4.min.js"></script>
  <script type="text/javascript" src="media/js/main.js"></script>
</body>
</html>