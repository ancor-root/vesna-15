<?php
define('APP', true);
require('../vendor/autoload.php');
require('inc/lib.php');

if ( ! empty($_SESSION['messages'])) {
	$messages = &$_SESSION['messages'];
} else {
	$messages = [];
	$_SESSION['messages'] = &$messages;
}

$user = isset($_SESSION['user']) ? $_SESSION['user'] : null;

$feedback = new \app\model\Feedback();

if ( ! empty($_POST)) {
	$text = isset($_POST['text']) ? $_POST['text'] : null;

	// Валидация
	// if ( !empty(...)) {}

	$result = $feedback->add($user['id'], $text);
	if ( $result ) {
		$messages[] = ['success', 'Добавлено'];
	} else {
		$messages[] = ['error', 'Не удалось добавить по неизвестным причинам'];
	}
}

$news = $feedback->index();




$content = render('feedback', [
	'isLoggedIn' => (bool)$user,
	'user'       => $user,
	'news'       => $news,
]);

$page = render('common', [
	'title' => 'Магазин игрушек',
	'content' => $content,
	'messages' => $messages,
]);

$_SESSION['messages'] = [];

echo $page;