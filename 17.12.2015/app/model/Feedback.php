<?php
namespace app\model;

class Feedback extends \app\inc\Db {

	public function index() {
		$sql = <<<SQL
			SELECT
				`f`.`id`,
			    `f`.`text`,
			    `f`.`created_at`,
			    `u`.`name`  AS `user_name`,
			    `u`.`email` AS `user_email`
			  FROM `feedback` AS `f`
			  INNER JOIN `user` AS `u` ON ( `u`.`id` = `f`.`user_id` )
SQL;
	
		$res = $this->db->query($sql);
		if ($res === false) {
			echo "ERRROR: $sql";
			die;
		}

		return $res->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function add($userId, $text) {
		$sql = sprintf("INSERT INTO `feedback` SET `user_id` = %s, `text` = %s, `created_at` = %d",
			$this->db->quote($userId),
			$this->db->quote($text),
			time()
		);

		$count = $this->db->exec($sql);

		if ($count === false) {
			echo "ERRROR: $sql";
			die;
		}

		return $count;
	}

}