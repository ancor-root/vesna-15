<?php
namespace app\model;

class User extends \app\inc\Db {
	const SUFFIX = '9283hd33d*&^&';

	public function __construct() {
		parent::__construct();
	}

	/**
	 * Создать пользователя
	 * @param  string $name  
	 * @param  string $email 
	 * @param  string $pwd   
	 * @return boolean        Создан?
	 */
	public function signup($name, $email, $pwd) {
		
		$sql = sprintf("INSERT INTO `user` SET `name` = %s, `email` = %s, `password_hash` = %s",
			$this->db->quote($name),
			$this->db->quote($email),
			$this->db->quote(md5($pwd . static::SUFFIX))
		);
		
		// echo "sql: $sql";

		$count = $this->db->exec($sql);

		return (bool)$count;
	}

	/**
	 * Авторизировать пользователя
	 * @param  string $email    
	 * @param  string $password 
	 * @return array            Пользователь
	 */
	public function login($email, $password) {
		$sql = sprintf("SELECT `id`, `name`, `email` FROM `user` WHERE `email` = %s AND `password_hash` = %s",
			$this->db->quote($email),
			$this->db->quote(md5($password . static::SUFFIX))
		);

	
		$res = $this->db->query($sql);
		if ($res === false) {
			echo "ERROR sql: $sql";
			die;
		}

		if ($res->rowCount() == 0) return null;

		return $res->fetch(\PDO::FETCH_ASSOC);
	}
}