<?php
namespace app\inc;

class Db {

	protected $db;

	public function __construct() {

		/* Подключение к базе данных ODBC, используя вызов драйвера */
		$dbName   = 'vesna_15';
		$user     = 'root';
		$password = '111';
		$host     = '127.0.0.1';
		$dsn      = "mysql:dbname={$dbName};host={$host}";

		try {
		    $this->db = new \PDO($dsn, $user, $password);
		} catch (\PDOException $e) {
		    echo 'Подключение не удалось: ' . $e->getMessage();
		    die;
		}

	}

}