<?php
session_start();

define('ROOT_DIR', dirname(__DIR__).'/');

function render($template, $params) {
	extract($params);

	ob_start();
	require(ROOT_DIR.'templates/'.$template.'.php');
	return ob_get_clean();
}