<?php
$menu = [

];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$title?></title>
	<style>
	header {
		border-bottom: 1px solid #333;
	}
	main {
		width: 1140px;
		margin: 20px auto;
	}
	footer {
		border-top: 1px solid #333;
	}
	.messages {
		font-weight: bold;
	}
	.message-success {
		color: green;
	}
	.message-error   {
		color: red;
	}
	.message-warning {
		color: orange;
	}
	</style>
</head>
<body>
	<header>
		<?=$title?>
		<menu>
			<?php foreach ($menu as $one): ?>
				<a href="<?=$item['link']?>"><?=$one['name']?></a>
			<?php endforeach ?>
		</menu>
	</header>
	<div class="messages">
		<?php foreach ($messages as $one): ?>
			<div class="message message-<?=$one[0]?>"><?=$one[1]?></div>
		<?php endforeach ?>
	</div>
	<main>
		<?=$content?>
	</main>
	<footer>
		&copy; Все права защищены
	</footer>
</body>
</html>