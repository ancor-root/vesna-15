<style>
	.news-one .date {
		color: #aaa;
		margin-left: 20px;
	}
	.form-new {
		margin-top: 50px;
	}
</style>
<div class="news">
	<?php foreach ($news as $one): ?>
		<div class="news-one">
			<div class="new-head">
				<span class="author"><?=$one['user_name']?> (<?=$one['user_email']?>) </span>
				<span class="date"><?=date('Y.m.d H:i:s', $one['created_at'])?></span>
			</div>
			<div class="new-content"><?=$one['text']?></div>
			<hr>
		</div>
	<?php endforeach ?>
</div>




<?php if ($isLoggedIn): ?>
	<div class="form-new">
		<a href="login.php?logout">Выход</a><br>
		<form action="feedback.php" method="POST">
			<p>email: <?=$user['email']?></p>
			<p>text: <textarea name="text" id="" cols="30" rows="10"></textarea></p>
			<p><button type="submit">Отправить</button></p>
		</form>
	</div>
<?php else: ?>
	Для того что бы оставить отзыв авторизируйтесь
	<a href="login.php">Войти</a>
<?php endif ?>