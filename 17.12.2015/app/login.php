<?php
define('APP', true);
require('../vendor/autoload.php');
require('inc/lib.php');

if ( ! empty($_SESSION['messages'])) {
	$messages = &$_SESSION['messages'];
} else {
	$messages = [];
	$_SESSION['messages'] = &$messages;
}
$_SESSION['messages'] = [];

if (isset($_GET['logout'])) {
	unset($_SESSION['user']);
	header('Location: login.php');
}

if ( ! empty($_POST)) {
	$email     = isset($_POST['email'])    ? $_POST['email']    : null;
	$password  = isset($_POST['password']) ? $_POST['password'] : null;

	$user = new \app\model\User();
	$user = $user->login($email, $password);
	$_SESSION['user'] = $user;

	if ( $user ) {
		$messages[] = ['success', 'Добро пожаловать, '.$user['name']];
		header('Location: feedback.php');
	} else {
		$messages[] = ['warning', 'Неверные данные'];
	}
}


$content = render('login', [

]);

$page = render('common', [
	'title' => 'Магазин игрушек',
	'content' => $content,
	'messages' => $messages,
]);

echo $page;