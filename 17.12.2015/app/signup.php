<?php
define('APP', true);
require('../vendor/autoload.php');
require('inc/lib.php');

// $user = new \app\model\User;
if ( ! empty($_SESSION['messages'])) {
	$messages = &$_SESSION['messages'];
} else {
	$messages = [];
	$_SESSION['messages'] = &$messages;
}

if ( ! empty($_POST)) {
	$name      = isset($_POST['name'])             ? $_POST['name']             : null;
	$email     = isset($_POST['email'])            ? $_POST['email']            : null;
	$password  = isset($_POST['password'])         ? $_POST['password']         : null;
	$password2 = isset($_POST['password_confirm']) ? $_POST['password_confirm'] : null;

	// Валидация
	if (empty($name)) {
		$messages[] = ['warning', 'Не заполнено имя'];
	}

	// Создаем пользователя
	if (empty($messages)) {
		$user = new \app\model\User();
		$res = $user->signup($name, $email, $password);
		if ($res) {
			$messages[] = ['success', 'Успешно зарегистрирован'];

			header('Location: login.php');
		} else {
			$messages[] = ['error', 'Не удалось создать по неизвестным причинам'];
		}
	}
}

$_SESSION['messages'] = [];


$content = render('signup', [

]);

$page = render('common', [
	'title'    => 'Магазин игрушек',
	'content'  => $content,
	'messages' => $messages,
]);

echo $page;