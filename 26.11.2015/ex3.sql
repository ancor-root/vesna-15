----------------------------------------------------------------------
PRIMARY KEY
----------------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` INT UNSIGNED,
  `name` VARCHAR(255)
);

INSERT INTO `user` SET `id` = 5, `name` = "ivan2222";
INSERT INTO `user` SET `id` = 5, `name` = "iva";


SELECT * FROM `user`;

1. Всегда есть
2. Всегда уникальный
3. Вечный
4. В одной таблице один первичный ключ


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` INT UNSIGNED PRIMARY KEY,
  `name` VARCHAR(255)
);

-- Второй способ задать PK
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` INT UNSIGNED,
  `name` VARCHAR(255),
  PRIMARY KEY (`id`)
);


-- Авто инкремент
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(255)
);

-- id не указываем
INSERT INTO `user` SET `name` = 'ivan';
INSERT INTO `user` SET `name` = 'Petro';


----------------------------------------------------------------------
UNIQUE KEY - обладает теми же характеристиками что и pk
----------------------------------------------------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` INT UNSIGNED,
  `email` VARCHAR(100),
  `name` VARCHAR(255),
  UNIQUE KEY(`email`)
);

INSERT INTO `user` SET
`id` = 1,
`name` = 'petro',
`email` = 'petro@mail.eu';



----------------------------------------------------------------------
NOT NULL            NULL не райно!! 0
----------------------------------------------------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(255),
  PRIMARY KEY (`id`)
);

INSERT INTO `user` SET `name` = "ivan2222";
INSERT INTO `user` SET `id` = 5;

SELECT * FROM `user`;



----------------------------------------------------------------------
 IF EXISTS                    IF NOT EXISTS
----------------------------------------------------------------------
SHOW WARNINGS;

DROP TABLE  `user`;
DROP TABLE IF EXISTS `user`;

CREATE TABLE IF NOT EXISTS `user` (
  `id` INT UNSIGNED,
  `name` VARCHAR(255)
);

----------------------------------------------------------------------
Движки(обзор)
----------------------------------------------------------------------
InnoDB
MyISAM - быстрее работает, нельзая задать связи

DROP TABLE  `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` INT UNSIGNED,
  `name` VARCHAR(255)
) ENGINE = InnoDB;







----------------------------------------------------------------------
Финальный пример
----------------------------------------------------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255),
  `email` VARCHAR(255),
  `age` TINYINT,
  PRIMARY KEY `pk_id`(`id`),
  UNIQUE KEY `unique_email`(`email`)
) ENGINE = InnoDB;


s-table
s-table-drop

s-s
s-ione

s-db
s-db-drop