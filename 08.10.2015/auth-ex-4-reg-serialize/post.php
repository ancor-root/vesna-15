<?php
require('lib.php');

dbLoad();

if ( isset($_GET['logout']) )
{
  logout();
  header('Location: auth.php');
  die;
}

$user = get_user();
if ( $user )
{
  echo "Hello {$user['name']}!";
  echo '<a href="post.php?logout">Выйти</a>';
}
else
{
  echo 'Access restrict';
}
