<?php
function get_users()
{
  static $users;

  if ( $users === null ) $users = require('db.php');

  return $users;
}

function get_user()
{
  $uid = isset($_COOKIE['UID']) ? $_COOKIE['UID'] : null;
  $users = get_users();

  foreach ($users as $login => $user ) {
    $hash = md5( $login . $user['pwd'] );

    if ( $uid == $hash ) return $user;
  }

  return false;
}

// function logged_in()
// {
//   $uid = isset($_COOKIE['UID']) ? $_COOKIE['UID'] : null;
//   $users = get_users();

//   foreach ($users as $login => $user ) {
//     $hash = md5( $login . $user['pwd'] );

//     if ( $uid == $hash ) return true;
//   }

//   return false;
// }

function login($login, $pwd)
{
  $users = get_users();

  // if ( ! isset($users[$login]) ) return false;
  // if ( $pwd != $users[$login]['pwd'] ) return false;

  if ( ! isset($users[$login]) || $pwd != $users[$login]['pwd'] ) return false;

  $hash = md5( $login . $pwd );
  setcookie('UID', $hash);

  return true;
}

// function login($login, $pwd)
// {
//   $users = get_users();
//   if ( isset($users[$login]) )
//   {
//     if ( $pwd == $users[$login]['pwd'] )
//     {
//       $hash = md5( $login . $pwd );
//       setcookie('UID', $hash);

//       return true;
//     }
//   }
 
//   return false;
// }

function logout()
{
  setcookie('UID', '', time()-60*60*24 );
}