
DROP TABLE IF EXISTS `book`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255)
) ENGINE = InnoDB;



INSERT INTO `author`(`name`) VALUES
('Kristel Gallivan'),
('Mavis Parchment'),
('Reatha Steese');

INSERT INTO `book`(`name`, `author_id`) VALUES
('Kumiss', 1),
('Episporangium', 1),
('Harmonically', 2),
('Stourness', NULL),
('Foliocellosis', 3),
('Triphenyl', 3);

SELECT `a`.* FROM `author` AS `a`;
SELECT `b`.* FROM `book` AS `b`;


SELECT
	`a`.*,
	`b`.*
FROM
	`author` AS `a`,
	`book` AS `b`;


SELECT
    `a`.*,
	`b`.*
  FROM `author` AS `a`
  INNER JOIN `book` AS `b` ON ( `b`.`author_id` = `a`.`id` )


SELECT
    `a`.*,
	`b`.*
  FROM `author` AS `a`
  LEFT JOIN `book` AS `b` ON ( `b`.`author_id` = `a`.`id` )
  WHERE `b`.`id` IS NULL



DELETE FROM `book` WHERE `author_id` = '2';


user
	id
	name

3 пользователя:
2 - имеют статьи
1 - нет статьи

article
	id
	title
	user_id - NOT NULL не нужно

- хотя бы одна статья без автора

1. Все из user, по возможности article
2. Все из article, по возможности user
3. Все что есть и там и там обязательно

CREATE TABLE IF NOT EXISTS `user` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255)
);