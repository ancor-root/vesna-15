<?php
function tasksLoad()
{
  global $config;
  static $tasks;
  if ( $tasks === null )
  {
    $tasksStr = file_get_contents("${config['root']}/data/${config['db']}");
    $tasks = unserialize($tasksStr);
  }

  return $tasks;
}

function tasksSave(array $tasks)
{
  global $config;
  $tasksStr = serialize($tasks);
  file_put_contents("${config['root']}/data/${config['db']}", $tasksStr);
}

function taskCreate($name)
{
  $tasks = tasksLoad();
  $tasks[] = [0, $name];
  tasksSave($tasks);
}

function taskDelete($id)
{
  $tasks = tasksLoad();
  unset($tasks[$id]);
  tasksSave($tasks);
}