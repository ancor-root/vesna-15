<?php
$count = 0;

// Пример кэширования на числе фибоначи
/*function fib($n)
{
  static $cache = [];

  $GLOBALS['count']++;

  if ( $n < 3 ) return 1;
  if ( isset($cache[$n]) ) return $cache[$n];

  return $cache[$n] = fib($n-1) + fib($n-2);
}

echo fib(15);
echo 'count: ' . $count;
// 1,1,2,3
*/

function tasksLoad()
{
  static $tasks;
  if ( $tasks === null )
  {
    $tasksStr = file_get_contents('todo-list/data/tasks.db');
    $tasks = unserialize($tasksStr);
  }

  return $tasks;
}

print_r( tasksLoad() );
print_r( tasksLoad() );
print_r( tasksLoad() );
print_r( tasksLoad() );
print_r( tasksLoad() );
print_r( tasksLoad() );
print_r( tasksLoad() );