<?php
define('APP', true);
require('../vendor/autoload.php');
require('inc/lib.php');

// $user = new \app\model\User;
$messages = [];

if ( ! empty($_POST)) {
	$name      = isset($_POST['name'])             ? $_POST['name']             : null;
	$email     = isset($_POST['email'])            ? $_POST['email']            : null;
	$password  = isset($_POST['password'])         ? $_POST['password']         : null;
	$password2 = isset($_POST['password_confirm']) ? $_POST['password_confirm'] : null;

	if (empty($name)) {
		$messages[] = 'Не заполнено имя';
	}
    if (empty($email)) {
		$messages[] = 'Не заполнено email';
	}
	 if ((strpos($email, "@") === false) || (strpos ($email, ".") === false)) {
		$messages[] = 'Email должен содержать @  и .';
	}
    if (empty($password)){
    	$messages[] = 'Не заполнено password';
    }
    if (empty($password2)){
    	$messages[] = 'Не заполнено password_confirm';
    }
    if (($password2) != ($password)){
	$messages[] = 'Не совпадают пароли';
}


	// Создаем пользователя
	if (empty($messages)) {
		$user = new \app\model\User();
		$res = $user->signup($name, $email, $password);
		if ($res) {
			$messages[] = 'Успешно зарегистрирован';
			// header('Location: login.php');
		} else {
			$messages[] = 'Не удалось создать по неизвестным причинам';
		}
	}
}

$content = render('signup', [

]);

$page = render('common', [
	'title'    => 'Магазин игрушек',
	'content'  => $content,
	'messages' => $messages,
]);

echo $page;