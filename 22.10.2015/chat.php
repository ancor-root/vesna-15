<?php
  define('MESSAGES_DB', 'chat.db.txt');

  if ( $_SERVER['REQUEST_METHOD'] == 'POST' )
  {
    $result = [
      'status' => true,
      'data'   => null,
    ];

    $actions = [

      'new-message' => function($data) {

        $message = $data['author'] . '::' . $data['text'] . "\n";
        file_put_contents(MESSAGES_DB, $message, FILE_APPEND);
      },

    ];

    $actionToRun = isset($_POST['action']) ? $_POST['action'] : null;
    $requestData = isset($_POST['data'])   ? $_POST['data']   : null;

    $result['data'] = $actions[$actionToRun]($requestData);

    header('Content-Type: text/json');
    echo json_encode($result);
    die;
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Chat v1</title>
  <style>
    .messages {
      margin-bottom: 25px;
      border-bottom: 3px dotted #444;
    }
    .message {
      margin-bottom: 15px;
    }
    textarea[name=message-text] {
      width: 300px;
    }
  </style>
</head>
<body>
  <div class="messages"></div>

  <form name="new">
    <input type="text" name="message-author">
    <p><textarea name="message-text"></textarea></p>
    <p><button type="submit">Send</button></p>
  </form>
  <script src="jquery-2.1.4.min.js"></script>
  <script src="main.js"></script>
</body>
</html>