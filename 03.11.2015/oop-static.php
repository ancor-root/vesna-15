<?php
class Car
{
	public static $count = 5;
	public static $distance = 0;

	public $model;
	public $speed;

	public function __construct($model, $speed)
	{
		$this->model = $model;
		$this->speed = $speed;

		// Car::$count++;
		static::$count++;
	}

	public function go($distance)
	{
		// Car::$distance = Car::$distance + $distance;
		// Car::$distance += $distance;
		static::$distance += $distance;

		return $distance / $this->speed;
	}
}

$car1 = new Car('mercedes', 200);
$car2 = new Car('mazda', 100);
$car3 = new Car('toyota', 300);

echo 'car1 проедет 1000 км за' . $car1->go(1000) . '<br>';
echo 'car2 проедет 1000 км за' . $car2->go(2000) . '<br>';
echo 'car3 проедет 1000 км за' . $car3->go(1000) . '<br>';

echo 'Создано машин:' . Car::$count . '<br>';
echo 'Общее расстояние:' . Car::$distance . '<br>';

/*
class Car
{
	public $count = 0;

	public $model;

	public function __construct($model)
	{
		$this->model = $model;
		$this->count++;
	}
}

$car1 = new Car('mercedes');
$car2 = new Car('mazda');
$car3 = new Car('toyota');

echo $car1->count . "<br>";
echo $car2->count . "<br>";
echo $car3->count . "<br>";
*/