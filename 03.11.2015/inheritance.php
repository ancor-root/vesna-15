<?php

class Rect
{
	protected $width;
	public $height;

	public function __construct($width, $height)
	{
		$this->width  = $width;
		$this->height = $height;
	}

	public function getArea()
	{
		return $this->width * $this->height;
	}
}

class Cube extends Rect
{
	public $length;

	public function __construct($width, $height, $length)
	{
		parent::__construct($width, $height);
		$this->length = $length;
		echo 'child : width: ' . $this->width . '<br>';
	}

	public function getValume()
	{
		return $this->getArea() * $this->length;
	}
}



/*
class myClass extends Cube {

	public function __construct($width, $height, $length, $param4)
	{
		parent::__construct($width, $height, $length);

	}
}*/

$rect1 = new Rect(10, 5);
$cube1 = new Cube(10, 10, 5);

// echo 'width: ' . $rect1->width . '<br>';

echo 'area:' . $rect1->getArea();
echo "<br>";
echo 'valume:' . $cube1->getValume();