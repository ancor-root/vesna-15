<?php

// function myFunc(/* args */)
function myFunc(...$args) // 5.6 only
{
	// $args = func_get_args();
	echo '<ul>';
	foreach ($args as $one) {
		echo "<li>$one</li>";
	}
	echo '</ul>';
}

myFunc('name1', 'name2', 'ivan');