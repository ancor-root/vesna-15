DROP TABLE IF EXISTS `user`;

CREATE TABLE IF NOT EXISTS `user` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255),
  `password_hash` VARCHAR(32),
  PRIMARY KEY `pk_id`(`id`)
) ENGINE = InnoDB;



INSERT INTO `user` SET
`name` = 'Ivan',
`password_hash` = MD5(CONCAT('%^FG%&*HUI', '111111'));

SELECT
    `u`.*
  FROM `user` AS `u`;


-- b1e973ae46088b42df2003dae689655f == md5('Y%FUa&*%GHasd^'.$password)