SELECT
  *
FROM `user`
WHERE `id` = '1';
		
INSERT INTO `user` SET `name` = 'Ivan1', `password_hash` = MD5(CONCAT('%^FG%&*HUI', '111111'));
INSERT INTO `user` SET `name` = 'Ivan2', `password_hash` = MD5(CONCAT('%^FG%&*HUI', '111111'));
INSERT INTO `user` SET `name` = 'Ivan3', `password_hash` = MD5(CONCAT('%^FG%&*HUI', '111111'));
INSERT INTO `user` SET `name` = 'Ivan4', `password_hash` = MD5(CONCAT('%^FG%&*HUI', '111111'));
INSERT INTO `user` SET `name` = 'Ivan5', `password_hash` = MD5(CONCAT('%^FG%&*HUI', '111111'));
INSERT INTO `user` SET `name` = 'Ivan6', `password_hash` = MD5(CONCAT('%^FG%&*HUI', '111111'));


SELECT
  *
FROM `user`
WHERE ( `id` BETWEEN 1 AND 3 ) OR ( `id` = 6 );

-- Операторы
=
!= <>
>
<
>=
<=
`id` IN (1,4,6)
`id` BETWEEN 1 AND 5

AND
OR

-- Ограничение выборки
LIMIT сколько просустить, сколько взять

SELECT
  *
FROM `user`
LIMIT 2*10, 10;

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255),
  `text` TEXT,
  `user_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY `pk_id`(`id`)
) ENGINE = InnoDB;


INSERT INTO `article`(`title`, `text`, `user_id`) VALUES
('cubanite', 'baldicoot iconomania truthlike unbravely preguard nonadmitted unechoing smuggle undissonant koph dyeleaves orective maggot nondenial kerugma corruptibility sniffily determinator unreverendly adipous Beulah unperplexing culvert sapan', 1),
('regalness', 'youngster zonality biotics cornic preoccasioned Cerialia overdeliciously overemptiness Tardigrada Sicilian aerodynamics anticorrosive unscotched overidealism birdy antilegalist unvituperated soppiness dihydrate macradenous grommet seigneurage fabiform unintended', 1),
('unrefrainable', 'clinger protopresbytery thoracolysis Ganocephala godmaking maxwell radiately mosaicism prologuer Hexanchidae Kalandariyah Telescopium Xylonite Ungulata repersuade tittymouse redoubted formee glycyl pervagation goodish alfilaria revolutionariness subelementary', 2),
('andrarchy', 'symphonia epigenesist Ruthene radiometric pinfeathery Christiana unrefused withholdal homoeopathician agreeing unsilvered bleachhouse tritium unpass laryngostomy deference skillful tone fetiparous lapideous Amelia overcutter nautical costula', 2),
('unbillet', 'theah ungueal unforestallable structuralize frumenty pinfold Braidist hygrostat bimetallistic sempiternal ungrounded proarchery triphenylated flighter glyph allwhither abstractor synonymy landlocked Euclid Siphonales skepticalness unisexuality concause', 3),
('unspecterlike', 'vine phallus desilicify staia pollenigerous sirenic midiron realness dijudicate laridine pamphletic skiograph vivaciousness equiradiate pycnia Pimpinella cardmaking anticontagion asclepidoid myxedemic Batis empiricalness Herulian sandaled', 5),
('carnose', 'espier nonplacet metabolism unancestored Samarkand Semnopithecus ungauged lightsomeness graphostatic analogism demulsify paleological uncaste replant chickenhearted specifically tophyperidrosis possibilitate Douglas nutlike superessential unthroning costicervical bruiter', 4),
('postprandial', 'octodentate maledict variolovaccinia nonschismatic occupation tarriness cadalene polypragmatist holodedron Farnovian Jacobinia kecksy nonexpiation irruptively coheir desk Trypeta beaterman pseudogenteel aware cymotrichous epigonos imbellious subjectivoidealistic', 6),
('stalk', 'beautifully ironback skippable outhasten spore unimmersed pseudodox Bartlett equiponderation quinamine bedtick Nemalionales grossify Jehovism runt seemless startlish fyke quassin notaeal predine anakinetomer trichloroacetic uncarved', 2);

INSERT INTO `comment`(`article_id`, `text`, `created_at`) VALUES
(2, 'lorem ipsum', '25.11'),
(4, 'lorem ipsum2', '25.12'),
(3, 'lorem ipsum3', '25.13'),
(1, 'lorem ipsum4', '25.14');
-- user_id, user_name, article_title

  
SELECT
    `a`.`name`        AS `article_author_name`,
	`a`.`title`       AS `article_title`,
	`c`.`text`        AS `comment_text`
   FROM `article` AS `a`
   INNER JOIN `user`    AS `a` ON ( `a`.`article_author_name` = `a`.`name` ),
   INNER JOIN `comment` AS `c` ON ( `c`.`article_id` = `a`.`id` );


 SELECT
     `a`.`user_id`     AS `article_author_id`,
     `a`.`title`       AS `article_title`,
     `c`.`text`        AS `comment_text`
    FROM `article` AS `a`
    INNER JOIN `comment` AS `c` ON ( `c`.`article_id` = `a`.`id` );


   
comment
	id
	article_id
	text
	created_at
	
CREATE TABLE IF NOT EXISTS `comment` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `article_id` INT UNSIGNED NOT NULL,
  `text` TEXT,
  `created_at` INT NOT NULL,
  PRIMARY KEY `comment_id`(`id`)
) ENGINE = InnoDB;
-- article_author_id article_title comment_text
-- article_author_name article_title comment_text






CREATE TABLE IF NOT EXISTS `author` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255),
  PRIMARY KEY `pk_id`(`id`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `book` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255),
  `author_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY `pk_id`(`id`)
) ENGINE = InnoDB;


INSERT INTO `author`(`name`) VALUES
('Kristel Gallivan'),
('Mavis Parchment'),
('Reatha Steese');

INSERT INTO `book`(`name`, `author_id`) VALUES
('Kumiss', 1),
('Episporangium', 1),
('Harmonically', 2),
('Stourness', 3),
('Foliocellosis', 3),
('Triphenyl', 3);

SELECT `a`.* FROM `author` AS `a`;
SELECT `b`.* FROM `book` AS `b`;


SELECT
	`a`.*,
	`b`.*
FROM
	`author` AS `a`,
	`book` AS `b`;


SELECT
    `a`.*,
	`b`.*
  FROM `author` AS `a`
  INNER JOIN `book` AS `b` ON ( `b`.`author_id` = `a`.`id` )



SET PASSWORD FOR 'root'@'localhost' = PASSWORD('111');