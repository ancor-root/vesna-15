<?php
/*
class A
{
	public function myMethod($n)
	{
		return $n*2;
	}
}

$obj = new A;
// echo $obj->myMethod(33);
$methodName = 'myMethod';
echo $obj->$methodName(55);

$caller = [$obj, 'myMethod'];
// echo $caller(22); // 5.6+
// echo call_user_func($caller, 22);
*/

/*
class Calc
{
	public function numberSum($n1, $n2)
	{
		return $n1 + $n2;
	}

	public function numberMultiple($n1, $n2)
	{
		return $n1 * $n2;
	}

	public function numberDevide($n1, $n2)
	{
		return $n1 / $n2;
	}
}


$n1 = isset($_GET['n1']) ? $_GET['n1'] : null;
// $n1 = $_GET['n1'] ?? null; // php7

$n2 = isset($_GET['n2']) ? $_GET['n2'] : null;
$operation = isset($_GET['operation']) ? $_GET['operation'] : null;


$myCalc = new Calc;
$operation = "number".ucfirst($operation);
echo "result: " . $myCalc->$operation($n1, $n2);

// http://localhost/05.11.2015/called-arrays.php?n1=5&n2=10&operation=multiple
*/

/*
class Pow
{
	public function __invoke()
	{
		echo "I invoked!";
	}
}

$myPow1 = new Pow;

echo $myPow1();
*/
/*
class Pow
{
	public function __invoke($name)
	{
		echo "Hello, $name!";
	}
}

$myPow1 = new Pow;

echo $myPow1('Ivan'); // Hello, Ivan!
*/

/*
class Pow
{
	public $n;

	public function __construct($n)
	{
		$this->n = $n;
	}

	public function __invoke()
	{
		return $this->n * $this->n;
	}
}

$myPow1 = new Pow(5);

echo $myPow1(); // 25
*/

/*
class Person
{
	public $name;

	public function __construct($name)
	{
		$this->name = $name;
	}

	public function __toString()
	{
		return "I am {$this->name}.";
	}
}

$person1 = new Person('Ivan');

echo $person1; // I am Ivan.
*/
