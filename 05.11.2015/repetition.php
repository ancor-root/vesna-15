<?php
class A
{
	private static $count = 0;

	public function __construct()
	{
		static::$count++;
	}

	public static function getCount()
	{
		return static::$count;
	}
}

class B extends A
{
	protected static $count = 0;
}

$a1 = new A;
$a2 = clone $a1;
$a3 = clone $a1;
$a4 = new A;

$b1 = new B;
$b2 = clone $b1;
$b3 = clone $b1;

echo "count of A: " . A::getCount() . '<br>';
echo "count of B: " . B::getCount() . '<br>';