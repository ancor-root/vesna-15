<?php

class Object
{
	private $name;

	public function __set($name, $value)
	{
		$method = "set".ucfirst($name);
		if ( method_exists($this, $method) )
		{
			$this->{$method}($value);
			// call_user_func([$this, $method], ...);
		}
		else
		{
			die('error: try to set undefined property');
		}
	}

	public function __get($name)
	{
		$method = "get".ucfirst($name);

		if ( ! method_exists($this, $method) ) return null;

		return $this->$method();
	}

	public function setName($name)
	{
		echo "setName was called! <br>";
		$this->name = ucfirst($name);
	}

	public function getName()
	{
		echo "getName was called! <br>";
		return $this->name;
	}
}


$person1 = new Person;

$person1->name = 'ivan';
echo $person1->name . "<br>";


// Фленаган - JavaScript подробное руководство(es5)
// var a = Object.defineProperty({}, 'name', {
// 	set : function($value) { console.log($value); }
// });