<?php
// self - там где написано
// static - последний наследник
class A
{
	private static $count = 0;

	public function __construct()
	{
		static::$count++;
	}

	public static function getCount()
	{
		return static::$count;
	}
}

class B extends A
{
	protected static $count = 0;
}

$a1 = new A;
$a2 = new A;
$a3 = new A;
$a4 = new A;

$b1 = new B;
$b2 = new B;
$b3 = new B;

echo "count of A: " . A::getCount() . '<br>';
echo "count of B: " . B::getCount() . '<br>';