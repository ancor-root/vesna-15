<?php
/*
class MyObject
{
	public $count = 0;

	public function __set($name, $value)
	{
		echo "try to write: `$name` = `$value`\n";
	}

	public function __get($name)
	{
		echo "try to read: `$name`\n";
	}
}

$obj1 = new MyObject;


print_r($obj1);
$obj1->name = 'hello';
$obj1->count = 5;
print_r($obj1);

echo $obj1->name . '<br>';
echo $obj1->age . '<br>';
*/


/*
class MyObject
{
	public $count = 0;
	private $properties = [];

	public function __set($name, $value)
	{
		$this->properties[$name] = $value;
	}

	public function __get($name)
	{
		return $this->properties[$name];
	}
}

$obj1 = new MyObject;


print_r($obj1);
$obj1->name = 'hello';
$obj1->count = 5;
print_r($obj1);

echo $obj1->name . '<br>';
echo $obj1->age . '<br>';
*/
/*
class MyObject
{
	public $count = 0;
	private $properties = [];

	public function __set($name, $value)
	{
		$this->properties[$name] = $value;
	}

	public function __get($name)
	{
		return $this->properties[$name];
	}

	public function __isset($name)
	{
		return isset( $this->properties[$name] );
	}

	public function __unset($name)
	{
		unset( $this->properties[$name] );
	}
}

$obj1 = new MyObject;


$obj1->name = 'hello';

var_dump( isset($obj1->name) );
echo $obj1->name . "\n\n";

unset($obj1->name);
var_dump( isset($obj1->name) );
echo $obj1->name . "\n";

*/
