<?php
/*
class Person
{
	public $name;
}


$user1 = new Person;
$user2 = $user1;
// $user2 = new Person;

$user1->name = 'Ivan';

echo $user2->name . '<br>';
$user2->name = 'Petro';

echo $user1->name . '<br>';
echo $user2->name . '<br>';
*/

/*
$n1 = 5;
$n2 = $n1;
// $n2 = 2;
echo "n1 : $n1 <br>";
echo "n2 : $n2 <br>";
echo "<br>";

$n1 = 6;
echo "n1 : $n1 <br>";
echo "n2 : $n2 <br>";
echo "<br>";

$n2 = 89;
echo "n1 : $n1 <br>";
echo "n2 : $n2 <br>";
*/

/*
// clone operator example
class User
{
	public $name;
}

$user1 = new User;
$user2 = clone $user1;
// $user2 = $user1;

$user1->name = 'ivan';
$user2->name = 'petro';
print_r($user1);
print_r($user2);
echo "<br>";
echo "<br>";

$user1->name = 'ivan22';
print_r($user1);
print_r($user2);
*/


/*
// __colne example
class Article
{
	public $title;

	public function __construct($title)
	{
		$this->title = $title;
	}

	public function __clone()
	{
		$this->title .= ' : copy 1';
	}
}

$article1 = new Article('about my dog');
$article2 = clone $article1;

print_r($article1);
echo "<br>";
print_r($article2);
*/

/*
class Article
{
	public $title;

	public function __construct($title)
	{
		$this->title = $title;
	}

	public function __clone()
	{
		if ( strpos($this->title, ' : copy ') !== false ) // суфикс найден
		{
			$spacePos = strrpos($this->title, ' ');
			$number = (int)substr($this->title, $spacePos+1);
			$numberLen = strlen($number);
			
			$this->title = substr($this->title, 0, -$numberLen) . ($number+1);
		}
		else
		{
			$this->title .= " : copy 1";
		}
	}
}

$article1 = new Article('about my dog');
$article2 = clone $article1;
$article3 = clone $article2;
$article4 = clone $article3;

echo "article1:".print_r($article1, 1)."<br>";
echo "article2:".print_r($article2, 1)."<br>";
echo "article3:".print_r($article3, 1)."<br>";
echo "article4:".print_r($article4, 1)."<br>";
*/