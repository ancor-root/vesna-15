<?php
// print_r($_SERVER['REQUEST_METHOD'])

// if ( ! empty($_POST) )
if ( $_SERVER['REQUEST_METHOD'] == 'POST' )
{
  $login = isset($_POST['login']) ? $_POST['login'] : null;
  $password = isset($_POST['password']) ? $_POST['password'] : null;
  $row = "$login:$password\n";
  file_put_contents('user-db.txt', $row, FILE_APPEND);
}
$usersStr = file_get_contents('user-db.txt');
$users = explode("\n", $usersStr);
array_pop($users);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
</head>
<body>
  <h4>users</h4>
  <ul>
    <? foreach($users as $key => $user): ?>
      <?php list($login, $password) = explode(':', $user) ?>
      <li><strong><?=$login?></strong> <?=$password?></li>
    <? endforeach ?>
  </ul>
  <hr>

  <form action="" method="POST">
    <p>login: <input type="text" name="login"></p>
    <p>password: <input type="text" name="password"></p>
    <p><button type="submit">Добавить</button></p>
  </form>

</body>
</html>