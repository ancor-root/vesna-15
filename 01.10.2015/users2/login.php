<?php
  $usersStr = file_get_contents('users-db.txt');
  $users = explode("\n", $usersStr);
  array_shift($users);
  array_pop($users);

  $sended = false;
  $isGuest = true;
  if ( isset($_POST['login-form']) )
  {
    $sended = true;
    $login    = $_POST['login'];
    $password = $_POST['password'];

    foreach ($users as $user) {
      $user = explode(':', $user);
      if ( $user[1] == $login && $user[2] == $password )
      {
        $isGuest = false;
        break;
      }
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>form 1</title>
</head>
<body>
    <?php if( $sended ): ?>
          
        <?php if( $isGuest ): ?>
          Incorrect login or password
        <?php else: ?>
          Wellcome
        <?php endif ?>

    <?php endif ?>
    
  <form action="" method="post">
    <p>login: <input type="text" name="login"></p>
    <p>password: <input type="text" name="password"></p>
    <p><button type="submit" name="login-form">Отправить</button></p>
  </form>
</body>
</html>