<?php

if ( ! file_exists('users-db.txt') ) 
{
  $template = "0\n";
  file_put_contents('users-db.txt', $template);
}

$usersStr = file_get_contents('users-db.txt');
$users = explode("\n", $usersStr);
array_pop($users);
$userLastId = array_shift($users);

if ( $_SERVER['REQUEST_METHOD'] == 'POST' )
{
  $login = isset($_POST['login']) ? $_POST['login'] : null;
  $password = isset($_POST['password']) ? $_POST['password'] : null;
  $userId = ++$userLastId;
  $row = "$userId:$login:$password\n";
  $users[] = $row;

  file_put_contents('users-db.txt', "$userLastId\n" . implode("\n", $users) );
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
</head>
<body>
  <h4>users</h4>
  <ul>
    <? foreach($users as $key => $user): ?>
      <?php list($id, $login, $password) = explode(':', $user) ?>
      <li><strong><?=$id?> </strong> <?=$login?> <?=$password?></li>
    <? endforeach ?>
  </ul>
  <hr>

  <form action="" method="POST">
    <p>login: <input type="text" name="login"></p>
    <p>password: <input type="text" name="password"></p>
    <p>password confirm: <input type="text" name="password"></p>
    <p><button type="submit">Добавить</button></p>
  </form>

</body>
</html>