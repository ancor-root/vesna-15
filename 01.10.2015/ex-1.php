<?php

$str = "qwertyewertasderter";

// Под строка
// echo substr($str, 2, 4); // erty
// echo substr($str, 3); // ertyrtyu
// echo substr($str, 0, -2); // qwert

// длина
// echo strlen($str); // 7

// Поиск подстроки
// echo strpos($str, 'ert'); // 2
// echo strpos($str, 'ert', 3); // 8

// Замена подстроки
// echo str_replace('ert', '$$$', $str); // qw$$$yew$$$asd$$$er

/*// Разбить по разделителю
$names_str = 'ivan,petr,kate,maria';
$names = explode(',', $names_str);
print_r($names);
*/

/*// Склеять массив в строку отделив элементы разделителем
$names_str = 'ivan,petr,kate,maria';
$names = explode(',', $names_str);
echo implode('|', $names);
*/
