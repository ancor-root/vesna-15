<?php
// Компиляция
// Интерпритация
// error_reporting( E_WARNING | E_NOTICE | E_ERROR |  );
// error_reporting( E_ALL );

// NOTICE - уведомление - Notice: 
// WARNING - предупреждение - Warning: 
// ERROR - ритическая -  Fatal error:

// PARSING - парсинг -  Parse error:

// foo();
// echo "asdasda";
// echo "adasd";
// if (1)
// {
//   echo 'msg 1';
  // echo $vaasdf;
//   echo 'msg 2';
// }
// file_get_contents('filename');
// $width = 10;
// $height = 10;
// print_r($arr3);

 // 0 * 2 = 0
 // 1 * 2 = 2
 // 2 * 2 = 4
 // 3 * 2 = 6

// echo "<pre>";
// rect($width, $height);
// echo "</pre>";


// /**
//  * Нарисовать прямоуггольник
//  * @param  integer $w Ширина прямоугольника
//  * @param  integer $h 
//  */
// function rect($w, $h)
// {
//   for($j = 0; $j < $h; $j++)
//   {
//     for($i = 0; $i < $w; $i++) echo '*';
//     echo "\n";
//   }
// }

// $num = 10;
// echo 20 - $num; // бинарный
// echo -$num; // унарный

/* 
$num = 10;
if ( $num == 10 )
{
  echo 'yes';
}
else
{
  echo 'no';
}
echo ( $num == 10 ) ? 'yes' : 'no';
*/

// var_dump( true && true ); // true
// var_dump( false && true );
// var_dump( true && false );
// var_dump( false && false );

// var_dump( true  || true );
// var_dump( false || true );
// var_dump( true  || false );
// var_dump( false || false ); // false

// echo "<pre>";
// rect($width, $height);
// echo "</pre>";

// /**
//  * Нарисовать прямоуггольник
//  * @param  integer $w Ширина прямоугольника
//  * @param  integer $h 
//  */
// function rect($w, $h)
// {
//   for($j = 0; $j < $h; $j++)
//   {
//     for($i = 0; $i < $w; $i++)
//     {
//       $isSpace = ( $j != 0 && $j != ($h-1) )   &&   ( $i != 0 && $i != ($w-1) );

//       echo $isSpace ? ' ' : '*';
//     }
//     echo "\n";
//   }
// }

// /**
//  * Нарисовать прямоуггольник
//  * @param  integer $w Ширина прямоугольника
//  * @param  integer $h 
//  */
// function rect($w, $h)
// {
//   for($j = 0; $j < $h; $j++)
//   {
//     for($i = 0; $i < $w; $i++)
//     {
//       echo (
//         ( $j != 0 && $j != ($h-1) ) &&
//         ( $i != 0 && $i != ($w-1) )
//       ) ? ' ' : '*';
//     }
//     echo "\n";
//   }
// }

// /**
//  * Нарисовать прямоуггольник
//  * @param  integer $w Ширина прямоугольника
//  * @param  integer $h 
//  */
// function rect($w, $h)
// {
//   for($j = 0; $j < $h; $j++)
//   {
//     for($i = 0; $i < $w; $i++)
//     {
//       if (
//         ( $j != 0 && $j != ($h-1) ) &&
//         ( $i != 0 && $i != ($w-1) )
//       ) {
//         echo ' ';
//       }
//       else
//       {
//         echo '*';
//       }
//     }
//     echo "\n";
//   }
// }

// /**
//  * Нарисовать прямоуггольник
//  * @param  integer $w Ширина прямоугольника
//  * @param  integer $h 
//  */
// function rect($w, $h)
// {
//   for($i = 0; $i < $w; $i++) echo '*';
//   echo "\n";


//   for($j = 0; $j < $h-2; $j++)
//   {
//     echo '*';
//     for($i = 0; $i < $w-2; $i++) echo ' ';
//     echo '*';
//     echo "\n";
//   }

//   for($i = 0; $i < $w; $i++) echo '*';
// }

// **********
// *        *
// *        *
// *        *
// *        *
// *        *
// *        *
// *        *
// *        *
// **********
// dz: 
echo "<table>";
echo "<tr>";
echo "<td>$i</td> <td>*</td> <td>$num</td> <td>=</td> <td>" . ($i*$num) . "</td>";
echo "</tr>";
echo "</table>";
?>