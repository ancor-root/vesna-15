<?php
namespace app;

class App
{
	public function __construct()
	{
		echo 'App construct fired<br><br>';
	}

	public function run()
	{
		$rect1 = new \ancor\app\Rect([
			'width'  => 50,
			'height' => 5,
		]);

		// $paralelepiped1 = new Paralelepiped(10, 10, 5);
		$paralelepiped1 = new \ancor\app\Paralel([
			'width'  => 10,
			'height' => 10,
			'length' => 5,
		]);

		echo 'area:' . $rect1->area;
		echo "<br>";
		echo 'valume:' . $paralelepiped1->valume;
	}
}