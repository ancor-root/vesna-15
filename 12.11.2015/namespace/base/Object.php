<?php
namespace base;

class Object
{
	public function __construct(array $properties)
	{
		foreach ($properties as $key => $value) {
			$this->$key = $value;
		}	
	}

	public function __set($name, $value)
	{
		$method = "set".ucfirst($name);
		if ( method_exists($this, $method) )
		{
			$this->{$method}($value);
		}
		else
		{
			die('error: try to set undefined property');
		}
	}

	public function __get($name)
	{
		$method = "get".ucfirst($name);

		if ( ! method_exists($this, $method) ) return null;

		return $this->$method();
	}
}