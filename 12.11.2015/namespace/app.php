<?php
require('base/Object.php');
require('app/Rect.php');
require('app/Paralel.php');

$rect1 = new \app\Rect([
	'width'  => 50,
	'height' => 51,
]);

// $paralelepiped1 = new Paralelepiped(10, 10, 5);
$paralelepiped1 = new \app\Paralel([
	'width'  => 10,
	'height' => 10,
	'length' => 51,
]);

echo 'area:' . $rect1->area;
echo "<br>";
echo 'valume:' . $paralelepiped1->valume;
