<?php
// for experiments
// $page = file_get_contents('data.txt');

$page = file_get_contents('http://sport.ua');
header('Content-Type: text/html; charset=windows-1251');
// header('Content-Type: text/plain; charset=windows-1251');

/**
 * regexp to search news block
 */
$pattern_ul = <<<REGEXP
/
\<ul[^\>]+id="[^\'"]*most_popular_news[^\'"]*"[^\>]*\>(.*)\<\/ul\>
/Uimsx
REGEXP;

/**
 * regexp to parse a new
 */
$pattern_item = <<<REGEXP
/
\<li\>
.*
\<a[^\>]+href\s*=\s*['"]([^'"]*)['"]\> # Группы 1 - ссылка
.*
  \<img[^>]+
    src\s*=\s*['"]([^'"]*)['"][^>]*    # Группа 2 - картинка
    alt\s*=\s*['"]([^'"]*)['"][^>]*    # Группа 3 - alt картинки
  \/\>
.*
\<\/a\>
.*
\<a[^\>]+class\s*=\s*['"][^'"]*pop-news__title[^'"]*['"][^\>]*\>(.*)\<\/a\> # Группа 4 - Заголовок
.*
\<div[^>]+class\s*=\s*['"][^'"]*meta__date[^'"]*['"][^>]*\>(.*)\<\/div\> # Группа 5 - время создания
.*
\<p[^>]*\>(.*)\<\/p\> # Группа 6 - текст
.*
\<\/li\>
/Uimsx
REGEXP;

/**
 * search a news block
 */
preg_match_all($pattern_ul, $page, $matches);
$ul = $matches[1][0];

/**
 * parse all news
 */
preg_match_all($pattern_item, $ul, $news, PREG_SET_ORDER);
array_walk($news, function(&$new) { unset($new[0]); });

/**
 * including the template script
 */
include "template.php";
